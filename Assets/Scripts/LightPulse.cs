﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightPulse : MonoBehaviour
{
    public float minIntensity = 0f;
    public float maxIntensity = 2f;
    public float pulseSpeed = 3f;

    private Light pulseLight;

    // Start is called before the first frame update
    void Start()
    {
        pulseLight = gameObject.GetComponent<Light>();
    }

    // Update is called once per frame
    void Update()
    {
        pulseLight.intensity = Mathf.Lerp(minIntensity, maxIntensity, Mathf.PingPong(Time.time * pulseSpeed, 1));
    }
}