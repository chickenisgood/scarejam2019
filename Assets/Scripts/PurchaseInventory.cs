﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PurchaseInventory : MonoBehaviour
{
    // Game Objects
    public GameObject inventoryObj;
    public GameObject inventoryCountTextObj;
    private GameObject toolTipTextObj;
    private Text NmFCostButtonText;

    // Qualifiers
    public PurchaseType purchaseType;
    public UpgradeType upgradeType;

    // Game Values
    public int NmFCost;
    public int SoulCost;
    public int inventoryLimit;
    public int radiusAroundVoid;

    // Event related
    private Button button;
    private EventTrigger eventTrigger;
    private bool mouseHover;

    // Count related
    private GameObject[] inventoryContainer;
    private int totalPurchased;
    private int totalHarvested;
    private float individualNmFPS;
    private float totalNmFPS;

    // Tool tip
    private string toolTipText;

    // Upgrade values
    private bool hasAutoPurchaseStarted;
    private bool hasSoulHarvestingStarted;

    // Pluralize text
    private Dictionary<string, string> pluralizerMap;

    // Enums
    public enum PurchaseType
    { 
        Inventory,
        Upgrade
    }

    public enum UpgradeType
    { 
        None,
        AutoHarvester,
        AutoPurchaser,
        Multiplier
    }

    // Controls the button's active state
    private bool Purchasable
    {
        get
        {
            switch (purchaseType)
            {
                case PurchaseType.Inventory:
                    return NmFGameManager.TotalNmF >= NmFCost && NmFGameManager.InventoryCount[inventoryObj.name] < inventoryLimit;
                case PurchaseType.Upgrade:
                    if (upgradeType == UpgradeType.AutoHarvester)
                    {
                        return NmFGameManager.TotalNmF >= NmFCost && NmFGameManager.AutoHarvesterLevel < inventoryLimit;
                    }
                    else if (upgradeType == UpgradeType.AutoPurchaser)
                    {
                        return NmFGameManager.TotalNmF >= NmFCost && NmFGameManager.AutoPurchaserLevel < inventoryLimit;
                    }
                    else if (upgradeType == UpgradeType.Multiplier)
                    {
                        return NmFGameManager.SoulCount >= SoulCost && NmFGameManager.SoulLevel <= inventoryLimit;
                    }
                    else
                    {
                        break;
                    }
            }

            return false;
        }
    }

    void Awake()
    {
        pluralizerMap = new Dictionary<string, string>()
        {
            { "Bunny", "Bunnies" },
            { "Chicken", "Chickens"},
            { "Puppy", "Puppies" },
            { "Goat", "Goats" }
        };
    }

    // Start is called before the first frame update
    void Start()
    {
        button = GetComponent<Button>();
        button.interactable = Purchasable;

        NmFCostButtonText = button.GetComponentInChildren<Text>();
        NmFCostButtonText.text = NmFCost.ToString();

        mouseHover = false;

        toolTipTextObj = GameObject.Find("ToolTipText");

        // Add reference to global inventory count
        NmFGameManager.InventoryCount.Add(inventoryObj.name, 0);

        inventoryContainer = new GameObject[inventoryLimit];
        totalPurchased = 0;

        // Event Trigger for hover icon
        eventTrigger = gameObject.GetComponent<EventTrigger>();

        if (purchaseType == PurchaseType.Inventory)
        {
            // Link Inventory purchase to button
            button.onClick.AddListener(PurchaseInventoryItem);

            // Get the NmF per second of the inventory item
            individualNmFPS = inventoryObj.GetComponent<InventoryUnit>().harvestNmFPerSecond;
        }
        else if (purchaseType == PurchaseType.Upgrade)
        {
            // Link Upgrade purchase to button
            button.onClick.AddListener(PurchaseUpgrade);
        }

        hasAutoPurchaseStarted = false;
        hasSoulHarvestingStarted = false;

        toolTipText = string.Empty;
    }

    // Update is called once per frame
    void Update()
    {
        button.interactable = Purchasable;

        if (purchaseType == PurchaseType.Inventory)
        {
            inventoryCountTextObj.GetComponent<TextMesh>().text = NmFGameManager.InventoryCount[inventoryObj.name].ToString() + "/" + inventoryLimit.ToString();

            // Update tool tip text
            totalHarvested = totalPurchased - inventoryContainer.Count(x => x != null);
            totalNmFPS = individualNmFPS * inventoryContainer.Count(x => x != null) + individualNmFPS * 2 * totalHarvested;
            toolTipText = "Sacrificed " + totalPurchased + " " + pluralizerMap[inventoryObj.name] + " producing " + totalNmFPS + " NmF/s\r\n";
            toolTipText += "Harvested " + totalHarvested + " " + inventoryObj.name + " souls";

            // Start auto purchasing the item
            if (NmFGameManager.AutoPurchaserEnabled && !hasAutoPurchaseStarted)
            {
                hasAutoPurchaseStarted = true;
                StartCoroutine(AutoPurchaseInventoryItem());
            }

            if (NmFGameManager.AutoHarvesterEnabled && !hasSoulHarvestingStarted)
            {
                hasSoulHarvestingStarted = true;
                StartCoroutine(AutoHarvestSoul());
            }
        }
        else if (upgradeType == UpgradeType.AutoHarvester)
        {
            inventoryCountTextObj.GetComponent<TextMesh>().text = "Lvl " + NmFGameManager.AutoHarvesterLevel;

            // Tool Tip with seconds written in the format 0.00
            if (NmFGameManager.AutoHarvesterEnabled)
            {
                toolTipText = "Harvesting a soul once every " + NmFGameManager.AutoHarvesterTime.ToString("N2") + " seconds";
            }
            else
            {
                toolTipText = "Unlock to auto harvest souls";
            }
        }
        else if (upgradeType == UpgradeType.AutoPurchaser)
        {
            inventoryCountTextObj.GetComponent<TextMesh>().text = "Lvl " + NmFGameManager.AutoPurchaserLevel;

            // Tool Tip with seconds written in the format 0.00
            if (NmFGameManager.AutoPurchaserEnabled)
            {
                toolTipText = "Sacrificing a unit once every " + NmFGameManager.AutoPurchaserTime.ToString("N2") + " seconds";
            }
            else
            {
                toolTipText = "Unlock to auto sacrifice with available NmF";
            }
        }
        else if (upgradeType == UpgradeType.Multiplier)
        {
            inventoryCountTextObj.GetComponent<TextMesh>().text = "Lvl " + NmFGameManager.SoulLevel;

            // Tool Tip with seconds written in the format 0.00
            if (NmFGameManager.SoulLevel > 0)
            {
                toolTipText = "Void producing " + NmFGameManager.NmFPerClick + " NmF/Click\r\n";
                toolTipText += "Free demons in " + (NmFGameManager.SoulLevelWinCount - NmFGameManager.SoulLevel) + " more levels!";
            }
            else if (NmFGameManager.SoulLevel == NmFGameManager.SoulLevelWinCount)
            {
                toolTipText = "FREE THE DEMONS";
            }
            else
            {
                toolTipText = "Unlock to open the void and gain additional NmF/click\r\n";
                toolTipText += "Free demons in " + (NmFGameManager.SoulLevelWinCount - NmFGameManager.SoulLevel) + " more levels!";
            }
        }

        if (mouseHover)
        {
            toolTipTextObj.GetComponent<TextMesh>().text = toolTipText;
        }
    }

    /// <summary>
    /// Checks if there are enough resources to purchase the item, then creates the object in the scene based on previously
    /// purchased inventory and count
    /// </summary>
    private void PurchaseInventoryItem()
    {
        if (Purchasable)
        {
            if (purchaseType == PurchaseType.Inventory)
            {
                // Add item to container
                for (int i = 0; i < inventoryLimit; i++)
                {
                    if (inventoryContainer[i] == null)
                    {
                        // Create the item
                        GameObject itemClone = Instantiate(inventoryObj);

                        // Set the item's position based on the index
                        itemClone.GetComponent<InventoryUnit>().SetPosition(i, inventoryLimit, radiusAroundVoid);

                        // Register the item's position. Item will be removed when the respective GameObject is destroyed xD
                        inventoryContainer[i] = itemClone;
                        break;
                    }
                }
            }

            ManageCount();
        }
    }

    /// <summary>
    /// Attempts to purchase inventory with available resources
    /// </summary>
    /// <returns></returns>
    private IEnumerator AutoPurchaseInventoryItem()
    {
        while (true)
        {
            PurchaseInventoryItem();
            yield return new WaitForSeconds(NmFGameManager.AutoPurchaserTime);
        }
    }

    /// <summary>
    /// Enables the corresponding upgrade if disabled and determines the time based on the upgrade level
    /// </summary>
    private void PurchaseUpgrade()
    {
        ManageCount();

        switch (upgradeType)
        {
            case UpgradeType.AutoHarvester:
                NmFGameManager.AutoHarvesterEnabled = true;
                NmFGameManager.AutoHarvesterTime = NmFGameManager.DefaultAutoHarvesterTime - Mathf.Sqrt(NmFGameManager.AutoHarvesterLevel);

                // Increase cost of future upgrades
                if (NmFGameManager.AutoHarvesterLevel < inventoryLimit)
                {
                    NmFCost = NmFCost + (int)Mathf.Pow(5f, NmFGameManager.AutoHarvesterLevel);
                    NmFCostButtonText.text = NmFCost.ToString();
                }
                else
                {
                    NmFCostButtonText.text = "Max";
                }
                break;
            case UpgradeType.AutoPurchaser:
                NmFGameManager.AutoPurchaserEnabled = true;
                NmFGameManager.AutoPurchaserTime = NmFGameManager.DefaultAutoPurchaserTime - Mathf.Sqrt(NmFGameManager.AutoPurchaserLevel);

                // Increase cost of future upgrades
                if (NmFGameManager.AutoPurchaserLevel < inventoryLimit)
                {
                    NmFCost = NmFCost + (int)Mathf.Pow(5f, NmFGameManager.AutoPurchaserLevel);
                    NmFCostButtonText.text = NmFCost.ToString();
                }
                else
                {
                    NmFCostButtonText.text = "Max";
                }

                break;
            case UpgradeType.Multiplier:
                // Increase click multiplier
                if (NmFGameManager.SoulLevel > NmFGameManager.SoulLevelWinCount)
                {
                    UnityEngine.SceneManagement.SceneManager.LoadScene("EndScene");
                }
                NmFGameManager.NmFPerClick *= NmFGameManager.NmFMultiplier;

                // Increase cost of future upgrades
                if (NmFGameManager.SoulLevel < NmFGameManager.SoulLevelWinCount)
                {
                    SoulCost = SoulCost + (int)Mathf.Pow(5f, NmFGameManager.SoulLevel);
                    NmFCostButtonText.text = SoulCost.ToString();
                }
                else
                {
                    SoulCost = 0;
                    
                    // Change button appearance to summon
                    NmFCostButtonText.text = "SUMMON!";
                    
                    NmFCostButtonText.alignment = TextAnchor.MiddleLeft;

                    // Hide the Soul icon
                    button.transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = false;
                }
                SoulBucket.IncrementVoidOpening();
                break;
        }
    }

    /// <summary>
    /// Keeps track of the inventory count and upgrade level. Also subtracts purchase cost from available NmF 
    /// </summary>
    private void ManageCount()
    {
        if (purchaseType == PurchaseType.Inventory)
        {
            // Increment item count
            if (NmFGameManager.InventoryCount.ContainsKey(inventoryObj.name))
            {
                NmFGameManager.InventoryCount[inventoryObj.name]++;
            }
            else
            {
                NmFGameManager.InventoryCount.Add(inventoryObj.name, 1);
            }
        }

        // Upgrade global upgrade level count
        switch (upgradeType)
        {
            case UpgradeType.AutoHarvester:
                NmFGameManager.AutoHarvesterLevel++;
                break;
            case UpgradeType.AutoPurchaser:
                NmFGameManager.AutoPurchaserLevel++;
                break;
            case UpgradeType.Multiplier:
                NmFGameManager.SoulLevel++;
                break;
            case UpgradeType.None:
                break;
        }

        // Add to total item count
        totalPurchased++;

        // Subtract available resources 
        if (upgradeType != UpgradeType.Multiplier)
        {
            NmFGameManager.TotalNmF -= NmFCost;
        }
        else
        {
            NmFGameManager.SoulCount -= SoulCost;
        }
    }

    /// <summary>
    /// Harvest souls with delay when auto-harvesting
    /// </summary>
    /// <returns></returns>
    private IEnumerator AutoHarvestSoul()
    {
        int harvestIndex = 0;
        int indicesChecked;

        while (true)
        {
            // Harvest the first available corrupt soul
            if (inventoryContainer.Count(x => x!= null) > 0)
            {
                indicesChecked = 0;

                while (harvestIndex < inventoryLimit && indicesChecked < inventoryLimit)
                {
                    if (inventoryContainer[harvestIndex] != null)
                    {
                        InventoryUnit unit = inventoryContainer[harvestIndex].GetComponent<InventoryUnit>();
                        if (unit.IsCorrupt && !unit.HasHarvestingStarted)
                        {
                            unit.HarvestSoul();
                            harvestIndex = (harvestIndex + 1) % inventoryLimit;
                            break;
                        }
                    }

                    harvestIndex = (harvestIndex + 1) % inventoryLimit;
                    indicesChecked++;
                }
            }

            yield return new WaitForSeconds(NmFGameManager.AutoHarvesterTime);
        }
    }

    public void DisplayToolTip()
    {
        mouseHover = true;
    }

    public void HideToolTip()
    {
        mouseHover = false;
        toolTipTextObj.GetComponent<TextMesh>().text = string.Empty;
    }
}