﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoverClickIcon : MonoBehaviour
{
    Texture2D cursor;
    Vector2 cursorHotspot;

    // Start is called before the first frame update
    void Start()
    {
        cursor = (Texture2D)Resources.Load(@"Cursor");
        cursorHotspot = Vector2.zero;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnMouseEnter()
    {
        Cursor.SetCursor(cursor, cursorHotspot, CursorMode.Auto);
    }

    private void OnMouseExit()
    {
        Cursor.SetCursor(null, cursorHotspot, CursorMode.Auto);
    }

    public void OnMouseEnterUI()
    {
        Cursor.SetCursor(cursor, cursorHotspot, CursorMode.Auto);
    }

    public void OnMouseExitUI()
    {
        Cursor.SetCursor(null, cursorHotspot, CursorMode.Auto);
    }
}
