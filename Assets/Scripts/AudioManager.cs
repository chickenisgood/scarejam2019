﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    AudioSource[] audioSources;
    private static int audioClipCounter = 0;

    public float fadeInTime = 2f;

    // Start is called before the first frame update
    void Start()
    {
        audioClipCounter = 0;
        audioSources = gameObject.GetComponents<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlayNextClip()
    {
        if (audioClipCounter < audioSources.Length)
        {
            audioSources[audioClipCounter].time = audioSources[0].time;
            StartCoroutine(FadeAudioIn(audioSources[audioClipCounter]));

            audioClipCounter++;
        }
    }

    /// <summary>
    /// Fade in an audio clip over time
    /// </summary>
    /// <param name="audioClipSource"></param>
    /// <returns></returns>
    private IEnumerator FadeAudioIn(AudioSource audioClipSource)
    {
        float elapsedTime = 0f;
        float startingVolume = 0f;
        float endingVolume = 1f;

        audioClipSource.volume = startingVolume;
        audioClipSource.Play();

        while (elapsedTime < fadeInTime)
        {
            // Increase volume over time
            audioClipSource.volume = Mathf.Lerp(startingVolume, endingVolume, (elapsedTime / fadeInTime));
            elapsedTime += Time.deltaTime;

            yield return new WaitForEndOfFrame();
        }

        audioClipSource.volume = endingVolume;
    }
}
