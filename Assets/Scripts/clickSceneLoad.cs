﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class clickSceneLoad : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void clickLoadEnd()
    {
        SceneManager.LoadScene("EndScene");   
    }

    public void clickLoadGame()
    {
        SceneManager.LoadScene("GameScene");
    }

    public void clickLoadBegin()
    {
        SceneManager.LoadScene("BeginScene");
    }

    public void clickQuit()
    {
        Application.Quit();
    }
}
