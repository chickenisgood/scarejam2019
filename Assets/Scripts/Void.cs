﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Void : MonoBehaviour, IPointerClickHandler
{
    public GameObject clickNumberObj;
    private GameObject clickNumberClone;
    private bool playFirstAudioClip;

    void Awake()
    {
        playFirstAudioClip = false;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /// <summary>
    /// Click event to handle increasing total Nightmare Fuel and creating temporary number text to indicate
    /// the quantity gained per click
    /// </summary>
    /// <param name="pointerEventData"></param>
    public void OnPointerClick(PointerEventData pointerEventData)
    {
        if (!playFirstAudioClip)
        {
            // Start next audio clip
            GameObject.Find("NmFGameManager").GetComponent<AudioManager>().PlayNextClip();
            playFirstAudioClip = true;
        }

        // Increase total count
        NmFGameManager.TotalNmF += NmFGameManager.NmFPerClick;

        // Spawn a text object representing the number
        if (NmFGameManager.ClickNumberPrefabQueue.Count > 0)
        {
            clickNumberClone = NmFGameManager.ClickNumberPrefabQueue.Dequeue();
        }
        else
        {
            clickNumberClone = Instantiate(clickNumberObj);
        }

        clickNumberClone.GetComponent<ClickNumber>().SetPostion(Input.mousePosition);
        StartCoroutine(clickNumberClone.GetComponent<ClickNumber>().FadeAndMoveUp());
    }
}
