﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NmFGameManager : MonoBehaviour
{
    // Time in between when the number of Nightmare Fuel per second is updated
    public float resourceConsumptionInterval;

    // Nightmare Fuel
    public static float TotalNmF { get; set; }
    public static float NmFPerS { get; set; }
    public static long NmFPerClick { get; set; }
    public static int NmFMultiplier { get; set; }

    // Soul
    public static int SoulCount { get; set; }
    public static int SoulLevel { get; set; }
    public static int SoulLevelWinCount { get; set; }

    // Upgrade Values
    public static bool AutoHarvesterEnabled { get; set; }
    public static int AutoHarvesterLevel { get; set; }
    public static float DefaultAutoHarvesterTime { get; set; }
    public static float AutoHarvesterTime { get; set; }
    public static bool AutoPurchaserEnabled { get; set; }
    public static int AutoPurchaserLevel { get; set; }
    public static float DefaultAutoPurchaserTime { get; set; }
    public static float AutoPurchaserTime { get; set; }

    // Store the count of each type of inventory by name
    public static Dictionary<string, int> InventoryCount { get; set; }

    // Queue to store and reuse instantiated click number prefabs
    public static Queue<GameObject> ClickNumberPrefabQueue { get; set; }

    private void Awake()
    {
        InventoryCount = new Dictionary<string, int>();

        ClickNumberPrefabQueue = new Queue<GameObject>();

        TotalNmF = 0;
        NmFPerS = 0;
        NmFPerClick = 1;
        NmFMultiplier = 5;

        SoulCount = 0;
        SoulLevel = 0;
        SoulLevelWinCount = 5;

        AutoHarvesterEnabled = false;
        AutoHarvesterLevel = 0;
        DefaultAutoHarvesterTime = 3.5f;
        AutoHarvesterTime = DefaultAutoHarvesterTime;

        AutoPurchaserEnabled = false;
        AutoPurchaserLevel = 0;
        DefaultAutoPurchaserTime = 3.5f;
        AutoPurchaserTime = DefaultAutoPurchaserTime;
    }

    private void Start()
    {
        StartCoroutine(GenerateResources());
    }

    public IEnumerator GenerateResources()
    {
        while (true)
        {
            TotalNmF += NmFPerS * resourceConsumptionInterval;
            yield return new WaitForSeconds(resourceConsumptionInterval);
        }
    }
}
