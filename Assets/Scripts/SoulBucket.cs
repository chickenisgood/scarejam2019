﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoulBucket : MonoBehaviour
{
    public GameObject soulBucketTextObj;
    private GameObject voidObj;
    private static LightPulse voidLightScript;

    private static SpriteRenderer pentagramSummonSprite;

    // Start is called before the first frame update
    void Start()
    {
        voidObj = GameObject.Find("Void");

        pentagramSummonSprite = GameObject.Find("PentagramSummon").GetComponent<SpriteRenderer>();
        pentagramSummonSprite.color = new Color(0, 0, 0, 0);

        voidLightScript = voidObj.GetComponentInChildren<Light>().GetComponent<LightPulse>();
    }

    // Update is called once per frame
    void Update()
    {
        /*
        if (IncreaseSoulVoid)
        {
            IncreaseSoulVoid = false;

            IncrementVoidOpening();

            //GameObject.Find("CombineSoulsButton").SetActive(false);
        }
        */
    }

    public static void IncrementVoidOpening()
    {

            //Set pentagram summoning opacity
            pentagramSummonSprite.color = new Color(0, 0, 0, 1 * ((float)NmFGameManager.SoulLevel / (float) NmFGameManager.SoulLevelWinCount));

            //Should check for condition to enable the "WIN" button
            //STUB Code
            //if(PortalState >= PortalFullyOpen)
            //{
            //    GameObject.Find("SummonButton").SetActive(true);
            //}

            // Affect Void visually
            voidLightScript.minIntensity = voidLightScript.maxIntensity;
            voidLightScript.maxIntensity += 50;

            // Start next audio clip
            GameObject.Find("NmFGameManager").GetComponent<AudioManager>().PlayNextClip();
        
    }
}
