Game Art Assets:

Cartoon Skeleton by sonild from https://opengameart.org/content/skeleton-warrior-2
Cartoon Zombie by irmirx from https://opengameart.org/content/zombie-animations
Cartoon Vampire by irmirx from https://opengameart.org/content/zombie-animations
Animal Sprites by Kenney.nl from https://opengameart.org/content/animal-pack
Pentagram Sprite from https://www.pngrepo.com/svg/102640/halloween-pentagram
