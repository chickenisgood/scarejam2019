﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickNumber : MonoBehaviour
{
    public float fadeTime = 2f;
    public float fadeDistance = 1f;

    private TextMesh textMesh;
    private MeshRenderer meshRenderer;

    private void Awake()
    {
        transform.SetParent(GameObject.Find("TextCanvas").transform, false);
        textMesh = gameObject.GetComponent<TextMesh>();
        meshRenderer = gameObject.GetComponent<MeshRenderer>();
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    /// <summary>
    /// Moves the number up, fades to transparent, and finally discards the prefab
    /// </summary>
    /// <returns></returns>
    public IEnumerator FadeAndMoveUp()
    {
        meshRenderer.enabled = true;

        // Set the text equal to the number of Nightmare Fuel per Click
        textMesh.text = "+" + NmFGameManager.NmFPerClick.ToString();

        float elapsedTime = 0;
        Vector3 startingPos = transform.position;
        Vector3 endPos = new Vector3(transform.position.x, transform.position.y + fadeDistance, transform.position.z);

        Color startingColor = textMesh.color;
        Color endingColor = new Color(startingColor.r, startingColor.g, startingColor.b, 0);

        while (elapsedTime < fadeTime)
        {
            // Move up
            transform.position = Vector3.Lerp(startingPos, endPos, (elapsedTime / fadeTime));
            elapsedTime += Time.deltaTime;

            // Make more transparent
            textMesh.color = Color.Lerp(startingColor, endingColor, (elapsedTime / fadeTime));
            yield return new WaitForEndOfFrame();
        }
        transform.position = endPos;
        textMesh.color = endingColor;

        meshRenderer.enabled = false;
        textMesh.color = startingColor;

        NmFGameManager.ClickNumberPrefabQueue.Enqueue(gameObject);
    }

    /// <summary>
    /// Sets the position of the GameObject
    /// </summary>
    /// <param name="mousePos"></param>
    public void SetPostion(Vector3 mousePos)
    {
        Ray castPoint = Camera.main.ScreenPointToRay(mousePos);
        RaycastHit hit;
        if (Physics.Raycast(castPoint, out hit, Mathf.Infinity))
        {
            transform.position = new Vector3(hit.point.x, hit.point.y + 1, hit.point.z);
        }
    }
}
