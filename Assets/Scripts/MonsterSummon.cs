﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class MonsterSummon : MonoBehaviour
{
    //doing quick and dirty monster loading because i'm lazy.
    public Sprite Vampire;
    public Sprite Skeleton;
    public Sprite Zombie;

    // Start is called before the first frame update
    void Start()
    {
        setUIMonsters();   
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void setUIMonsters()
    {
        int rndMonster = Random.Range(1, 3);
        Image img = GameObject.Find("MonsterImage").GetComponent<Image>();
        Text txt = GameObject.Find("MonsterDescription").GetComponent<Text>();

        switch (rndMonster)
        {
            //case 1: Vampire
            case 1:
                img.sprite = Resources.Load<Sprite>("Vampire");
                txt.text = "Congratulations! You have summoned the VAMPIRE. One of the most deadly creatures of the night, rest assured that the world will quickly fall to the Vampire and its minions.";
                break;
            //case 2: Skeleton
            case 2:
                img.sprite = Resources.Load<Sprite>("Skeleton");
                txt.text = "Congratulations! You have summoned the SKELETON. This one isn't so deadly by itself, but it is deadly in great numbers! When you find one wandering skeleton, you will surely find more. The world will rue the day that they crossed you!";
                break;

            //case 3: Zombie
            case 3:
                img.sprite = Resources.Load<Sprite>("Zombie");
                txt.text = "Congratulations! You have summoned the ZOMBIE. Slow, dumb, but can't feel pain and has an insatiable hunger for brains and flesh. Like a wildfire, an unchecked zombie will exponentially multiply and quickly devour the world. Good work there, summoner.";
                break;

            default:
                img.sprite = Resources.Load<Sprite>("Vampire");
                txt.text = "Congratulations! You have summoned the VAMPIRE. One of the most deadly creatures of the night, rest assured that the world will quickly fall to the Vampire and its minions.";
                break;

        }

    }
}
