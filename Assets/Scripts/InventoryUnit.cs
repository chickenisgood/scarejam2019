﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class InventoryUnit : MonoBehaviour, IPointerClickHandler
{
    // Resource variables
    public float harvestNmFPerSecond;
    public float timeUntilCorruption;
    public int soulPoints;
    private float timeSinceInitialization;

    // Position variables
    private float movementSpeed = 1f;
    private float movementAngle = 0f;
    private int inventoryIndex;
    private int inventoryLimit;
    private GameObject voidObj;
    public float fadeTime = 1.5f;
    public float fadeDistance = 0.5f;

    // Appearance
    private SpriteRenderer spriteRenderer;
    public Sprite corruptSprite;
    public Sprite spiritSprite;

    // Event variables
    public bool IsCorrupt { get; set; }
    public bool HasHarvestingStarted { get; set; }
    private HoverClickIcon iconScript;

    // Tool tip
    GameObject toolTipObj;

    void Awake()
    {
        voidObj = GameObject.Find("Void");
        IsCorrupt = false;
        HasHarvestingStarted = false;
        iconScript = gameObject.GetComponent<HoverClickIcon>();
        timeSinceInitialization = 0f;
    }

    // Start is called before the first frame update
    void Start()
    {
        toolTipObj = GameObject.Find("ToolTipText");
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        StartCoroutine(ConsumeResource());
    }

    // Update is called once per frame
    void Update()
    {
        movementAngle = movementSpeed * Time.time + (5f * inventoryIndex / inventoryLimit);
        Vector3 newPos = new Vector3(transform.position.x, transform.position.y, 0.5f * Mathf.Sin(movementAngle));
        transform.position = newPos;

        if (!spriteRenderer.enabled)
        {
            spriteRenderer.enabled = true;
        }

        iconScript.enabled = IsCorrupt;

        timeSinceInitialization += Time.deltaTime;
    }

    public void SetPosition(int positionIndex, int totalPositions, int radiusAroundVoid)
    {
        this.inventoryIndex = positionIndex;
        this.inventoryLimit = totalPositions;
        float angle = positionIndex * 2f * Mathf.PI / totalPositions;
        float zDimAngle = movementSpeed * Time.time + (5f * inventoryIndex / inventoryLimit);
        Vector3 newPos = new Vector3(Mathf.Sin(angle), Mathf.Cos(angle), 0.5f * Mathf.Sin(zDimAngle)) * radiusAroundVoid + voidObj.transform.position;
        transform.position = newPos;
    }

    IEnumerator ConsumeResource()
    {
        // Add the item's harvest to the global harvest
        NmFGameManager.NmFPerS += harvestNmFPerSecond;

        yield return new WaitForSeconds(timeUntilCorruption);

        // Change appearance based on corruption
        IsCorrupt = true;
        spriteRenderer.sprite = corruptSprite;
        gameObject.GetComponent<Light>().enabled = true;
        gameObject.GetComponent<LightPulse>().enabled = true;
    }

    public void OnPointerClick(PointerEventData pointerEventData)
    {
        if (IsCorrupt && !HasHarvestingStarted)
        {
            HarvestSoul();
        }
    }

    /// <summary>
    /// Removes item while collecting its souland gives bonuses 
    /// </summary>
    public void HarvestSoul()
    {
        // Denote that this object should not start the harvesting process another time
        HasHarvestingStarted = true;

        // Decrement item count
        NmFGameManager.InventoryCount[gameObject.name.Replace("(Clone)", "")]--;

        // "Double" the output after corruption
        NmFGameManager.NmFPerS += harvestNmFPerSecond;

        // Increase total collected count
        NmFGameManager.SoulCount += soulPoints;

        // Force remove click icon
        iconScript.OnMouseExitUI();

        // Launch spirit "animation," which eventually destorys the game object
        StartCoroutine(ConvertToSpirit());
    }

    /// <summary>
    /// Changes the unit sprite, "animates," and removes the game object
    /// </summary>
    /// <returns></returns>
    private IEnumerator ConvertToSpirit()
    {
        // Change the sprite to the spirit sprite
        spriteRenderer.sprite = spiritSprite;

        // Change the color of the point light to white
        gameObject.GetComponent<Light>().color = Color.white;

        float elapsedTime = 0;
        Vector3 startingPos = transform.position;
        Vector3 endPos = new Vector3(transform.position.x, transform.position.y + fadeDistance, transform.position.z);

        // Go from the original color to transparent
        Color startingColor = spriteRenderer.color;
        Color endingColor = new Color(startingColor.r, startingColor.g, startingColor.b, 0);

        // Start wobble randomly either left or right
        int[] randomDirections = new int[] { -1, 1 };
        int randomDirection = randomDirections[Random.Range(0, randomDirections.Length)];

        while (elapsedTime < fadeTime)
        {
            // Dampened wobble - e^(-t)*sin*2*pi*t)
            float newXPos = startingPos.x + 0.5f * randomDirection * Mathf.Exp(-1 * elapsedTime / fadeTime) * Mathf.Sin(elapsedTime * 2 * Mathf.PI / fadeTime);

            // Move up
            float newYPos = Mathf.Lerp(startingPos.y, endPos.y, (elapsedTime / fadeTime));

            Vector3 newPos = new Vector3(newXPos, newYPos, transform.position.z);
            transform.position = newPos;
            elapsedTime += Time.deltaTime;

            // Make more transparent
            spriteRenderer.color = Color.Lerp(startingColor, endingColor, (elapsedTime / fadeTime));
            yield return new WaitForEndOfFrame();
        }

        transform.position = endPos;
        spriteRenderer.color = endingColor;

        // Finally destroy the object
        Destroy(gameObject);
    }

    private void OnMouseOver()
    {
        if (!IsCorrupt)
        {
            toolTipObj.GetComponent<TextMesh>().text = "+" + harvestNmFPerSecond + "NmF/s\r\n";
            toolTipObj.GetComponent<TextMesh>().text += "Harvestable in " + (timeUntilCorruption - timeSinceInitialization).ToString("N2") + " seconds";
        }
        else
        {
            toolTipObj.GetComponent<TextMesh>().text = "+" + soulPoints + " soul points and +" + harvestNmFPerSecond + " NmF/s";
        }
        
    }

    private void OnMouseExit()
    {
        toolTipObj.GetComponent<TextMesh>().text = string.Empty;
    }
}